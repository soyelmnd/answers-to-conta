angular.module('conta').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
        return {
            request: function(config) {
                return config;
            }
            requestError: function(rejection) {
                return $q.reject(rejection);
            },
            response: function(response) {
                // Instead of using error code
                //   we're using a `success` field
                //   in the response
                if (!response.data.success) {
                    return $q.reject(response.data);
                }

                // And always resolve data
                //   so we don't have to care
                //   whether it's $http.success
                //   or $q.then
                return response.data;
            },
            responseError: function(rejection) {
                return $q.reject(rejection);
            }
        }
    })
}]);

angular.module('conta').factory('user', ['$http', function (AuthInterceptor) {
    var baseApiUrl = '/api/v1/'; // @todo move to config or something, to update easily

    return {
        /**
         * Create a new user
         * @param {Object} data
         * @param {String} token
         * @returns {promise}
         */
        createUser: function (data, token) {
            return $http({
                method: 'POST',
                url: baseApiUrl + '/user/create',
                data: data,
                headers: {
                    'Auth-Token': token,

                    // If this content-type is used all the time
                    //  then we should set it default in $httpProvider as well
                    //  but I don't really think it should
                    //
                    // A JSON request body will be easy to maintain and handy
                    //  as compared to a classic key-value form urlencoded.
                    //  Is this the `backend improvement` question related?
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },

        /**
         * Change password
         * @param {Object} data
         * @param {String} token
         * @returns {promise}
         */
        changePassword: function (data, token) {
            return $http({
                method: 'POST',
                url: baseApiUrl + '/user/change-password',
                data: data,
                headers: {
                    'Auth-Token': token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}]);
