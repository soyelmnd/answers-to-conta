## Questions
1. A counter should increase by one on each click in [jsbin.com/boyufo](http://jsbin.com/boyufo)

- Why does it not work?
- If you only could add code, what would you add to make the counter work?
- You can get full overview of the code here: [jsbin.com/boyufo/edit](http://jsbin.com/boyufo/edit)

> Answer: [jsbin.com/sumofe/edit](http://jsbin.com/sumofe/edit)

2. If you write your first name in [jsbin.com/lezore](http://jsbin.com/lezore) this is reflected in the edit input.

- If you edit your name in the edit input:
    1. The first input is not updated.
    2. The first input is no longer updating the second input.
- What is happening and how do we fix it?
- What actions or rules can we take to prevent this in the future?
- You can get full overview of the code here: [jsbin.com/lezore/edit](http://jsbin.com/lezore/edit)

> Answer: [jsbin.com/tedudu/edit](http://jsbin.com/tedudu/edit)

3. A list is updated after two seconds in [jsbin.com/decutu](jsbin.com/decutu) using `$timeout`.

- The view is however not updated after two seconds.
- What is happening and how do we fix it?
- What actions or rules can we take to prevent this in the future?
- You can get full overview of the code here: [jsbin.com/decutu/edit](http://jsbin.com/decutu/edit).

> Answer: [jsbin.com/wevice/edit](http://jsbin.com/wevice/edit)


## Coding assignments
### User Service
Check out the folder `user-service` and open `user.service.js`

- What improvements could be done on this service and generally on frontend?
- What improvements could be done on backend?

### Unit testing
Check out the folder `unit-testing`.

- Install with `npm install` in a terminal.
- You can run the test suite with `npm run test`.
- You have to have Chrome installed in order to make this work.

There are 5 tests that fails.  
Fix the issues in `product.model.js` without changing the spec. When all tests are green, continue.  

Add a function `setTaxRate` to set the tax rate.  
Throw an error if the tax rate is not a number or if it is lower than 0 and higher than 100.  
Add tests that covers all requirements.
