(function () {
    'use strict';

    angular.module('conta').factory('Product', ProductModel);

    /**
     * @ngdoc factory
     * @name ProductModel
     */
    function ProductModel() {
        /**
         * Defaults
         */
        var defaults = {
            taxRate: 15
        };

        /**
         * Product Constructor.
         *
         * @param {Object} properties
         * @constructor
         */
        function Product(properties) {
            // Merge properties with default values
            properties = angular.extend({}, defaults, properties);
            for(var k in properties) {
                this[k] = properties[k];
            }
        }

        /**
         * Public
         */
        angular.extend(Product.prototype, {
            getPriceIncVat: getPriceIncVat,
            setTaxRate: setTaxRate
        });

        /**
         * Return constructor
         */
        return Product;

        /**
         * Get price incl. vat.
         *
         * @returns {Number}
         */
        function getPriceIncVat() {
            return Math.round((this.price + (this.price * this.taxRate / 100)) * 100) / 100;
        }

        /**
         * Set tax rate
         *
         * @param {Number|string} taxRate
         * @returns {Product} this
         * @throws {Error} Out of range
         * @throws {Error} NaN
         */
        function setTaxRate(taxRate) {
            if(isNaN(taxRate)) {
                throw new Error('Nan, only number please');
            }

            // Keep in mind that we should support
            //   number-convertable strings as well
            if('string' == typeof taxRate) {
                taxRate = parseFloat(taxRate);
            }

            if(taxRate < 0 || taxRate > 100) {
                throw new Error('Out of range, only 0-100 please');
            }

            this.taxRate = taxRate;

            return this;
        }
    }
}());
