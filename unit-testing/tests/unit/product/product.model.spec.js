describe('Model: Product', function () {
    var product, model, Product;

    beforeEach(function () {
        module('conta');

        inject(function ($injector) {
            Product = $injector.get('Product');
        });

        model = {
            name: 'Beer',
            taxRate: 8.975,
            price: 19.99
        };

        product = new Product(model);
    });

    it('should be an instance of Product', function () {
        expect(product instanceof Product).toBeTruthy();
    });

    describe('on initialization', function () {
        it('should set default values on an empty product', function () {
            product = new Product();

            expect(product.taxRate).toBe(15);
        });

        it('should set model on the product', function () {
            expect(product.name).toBe('Beer');
            expect(product.price).toBe(19.99);
            expect(product.taxRate).toBe(8.975);
        });

        it('should override any default values', function () {
            model = { taxRate: 55 };

            product = new Product(model);

            expect(product.taxRate).toBe(55);
        });

        it('should set default values on the product if they are missing', function () {
            model = { name: 'Beer' };

            product = new Product(model);

            expect(product.name).toBe(model.name);
            expect(product.taxRate).toBe(15);
        });
    });

    describe('getPriceIncVat()', function () {
        it('should return the price incl. VAT with two decimals', function () {
            expect(product.getPriceIncVat()).toBe(21.78);
        });
    });

    describe('setTaxRate()', function () {
        it('should be able to set the taxRate', function () {
            product.price = 10;

            product.setTaxRate(10);
            expect(product.getPriceIncVat()).toBe(11);

            product.setTaxRate(50);
            expect(product.getPriceIncVat()).toBe(15);
        });

        it('should block out-of-range number', function () {
            expect(function() { product.setTaxRate(-1) }).toThrowError(/out of range/i);
            expect(function() { product.setTaxRate(101) }).toThrowError(/out of range/i);
            expect(function() { product.setTaxRate(0) }).not.toThrow();
            expect(function() { product.setTaxRate(100) }).not.toThrow();
            expect(function() { product.setTaxRate(50) }).not.toThrow();
        });

        it('should block NaN, but accept number-convertable string', function () {
            expect(function() { product.setTaxRate('a') }).toThrowError(/nan/i);
            expect(function() { product.setTaxRate('10a') }).toThrowError(/nan/i);
            expect(function() { product.setTaxRate(10) }).not.toThrow();
            expect(function() { product.setTaxRate('10') }).not.toThrow();
        });
    });
});
